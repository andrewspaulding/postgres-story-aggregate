#!/bin/bash

# Delete Debezium source connector

curl -s -X DELETE 127.0.0.1:8083/connectors/postgres_story_source

# Delete JDBC sink connector

curl -s -X DELETE 127.0.0.1:18083/connectors/postgres_story_sink