#!/bin/bash

# Create Debezium source connector

curl -i -X POST \
    -H "Accept:application/json" \
    -H  "Content-Type:application/json" \
    http://localhost:8083/connectors/ -d '
    {
        "name": "postgres_story_source",
        "config": {
            "connector.class": "io.debezium.connector.postgresql.PostgresConnector",
            "tasks.max": "1",
            "database.hostname": "postgres-story-aggregate",
            "database.port": "5432",
            "database.user": "postgres",
            "database.password": "postgres",
            "database.dbname" : "postgres",
            "database.server.name": "postgres",
            "table.whitelist": "public.story",
            "transforms": "unwrap,InsertTopic",
            "transforms.unwrap.type": "io.debezium.transforms.ExtractNewRecordState",
            "transforms.unwrap.drop.tombstones": "false",
            "transforms.unwrap.delete.handling.mode": "rewrite",
            "transforms.InsertTopic.type":"org.apache.kafka.connect.transforms.InsertField$Value",
            "transforms.InsertTopic.topic.field":"messagetopic"
        }
    }'

# Create JDBC sink connector

curl -X "POST" "http://localhost:18083/connectors/" \
     -H "Content-Type: application/json" \
     -d '{
             "name": "postgres_story_sink",
             "config": {
                 "connector.class": "io.confluent.connect.jdbc.JdbcSinkConnector",
                 "connection.url": "jdbc:postgresql://postgres-story-aggregate:5432/postgres?user=postgres&password=postgres",
                 "auto.create":"false",
                 "auto.evolve":"true",
                 "insert.mode":"upsert",
                 "delete.enabled": "true",
                 "pk.mode": "record_key",
                 "pk.fields": "id",
                 "table.name.format": "story_profile",
                 "topics": "postgres.public.profile",
                 "transforms": "ReplaceField",
                 "transforms.ReplaceField.type": "org.apache.kafka.connect.transforms.ReplaceField$Value",
                 "transforms.ReplaceField.whitelist": "username,name,__deleted"
             }
     }'